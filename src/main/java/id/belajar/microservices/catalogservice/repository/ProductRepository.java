package id.belajar.microservices.catalogservice.repository;

import id.belajar.microservices.catalogservice.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, String> {
}

