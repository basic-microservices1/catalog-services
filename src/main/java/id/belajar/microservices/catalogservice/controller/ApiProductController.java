package id.belajar.microservices.catalogservice.controller;

import id.belajar.microservices.catalogservice.entity.Product;
import id.belajar.microservices.catalogservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product/api")
public class ApiProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/")
    public Page<Product> findProducts(Pageable page){
        return productRepository.findAll(page);
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product p) {
        return p;
    }

}
